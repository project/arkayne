// $Id: README.txt,v 1.0 2010/11/20 19:34:53 kevinwal Exp $

Welcome to Arkayne. This module connects to Arkayne and provides relevant links and SEO to your website

Installing Arkayne:

1) Uncompress the module and place the module in /sites/all/modules/
2) Configure the module and enter in your Arkayne key at /admin/settings/arkayne
3) Configure permissions for Arkayne at /admin/user/permissions

